use crate::tile::Orientation::*;
use crate::vector2::Vector2i;

pub struct Tile {
    positions_0: Vec<Vector2i>,
    positions_90: Vec<Vector2i>,
    positions_180: Vec<Vector2i>,
    positions_270: Vec<Vector2i>,
    orientation: Orientation,
}

enum Orientation {
    North,
    East,
    South,
    West,
}

impl Orientation {
    fn rotate(orientation: &Orientation, clockwise: bool) -> Orientation {
        if clockwise {
            match orientation {
                North => East,
                East => South,
                South => West,
                West => North,
            }
        } else {
            match orientation {
                North => West,
                West => South,
                South => East,
                East => North,
            }
        }
    }
}

impl Tile {
    pub fn random_tile() -> Tile {
        match rand::random::<u8>() % 7 {
            0 => { Tile::generate_square() }
            1 => { Tile::generate_long() }
            2 => { Tile::generate_t() }
            3 => { Tile::generate_s() }
            4 => { Tile::generate_s_inverse() }
            5 => { Tile::generate_l() }
            6 => { Tile::generate_l_inverse() }
            _ => { Tile::empty_tile() }
        }
    }

    pub fn get_positions(&self) -> &Vec<Vector2i> {
        match self.orientation {
            North => &self.positions_0,
            East => &self.positions_90,
            South => &self.positions_180,
            West => &self.positions_270,
        }
    }

    pub fn rotate(&mut self, clockwise: bool) {
        self.orientation = Orientation::rotate(&self.orientation, clockwise);
    }

    fn empty_tile() -> Tile {
        Tile {
            positions_0: vec![],
            positions_90: vec![],
            positions_180: vec![],
            positions_270: vec![],
            orientation: North,
        }
    }

    fn generate_square() -> Tile {
        let mut tile = Tile::empty_tile();

        tile.positions_0 = Self::convert_positions(vec![[0, 0], [0, 1], [1, 0], [1, 1]]);
        tile.positions_90 = tile.positions_0.clone();
        tile.positions_180 = tile.positions_0.clone();
        tile.positions_270 = tile.positions_0.clone();

        tile
    }

    fn generate_long() -> Tile {
        let mut tile = Tile::empty_tile();

        tile.positions_0 = Self::convert_positions(vec![[0, -1], [0, 0], [0, 1], [0, 2]]);
        tile.positions_90 = Self::convert_positions(vec![[-1, 0], [0, 0], [1, 0], [2, 0]]);
        tile.positions_180 = tile.positions_0.clone();
        tile.positions_270 = tile.positions_90.clone();

        tile
    }

    fn generate_t() -> Tile {
        let mut tile = Tile::empty_tile();

        tile.positions_0 = Self::convert_positions(vec![[-1, 1], [0, 1], [1, 1], [0, 0]]);
        tile.positions_90 = Self::convert_positions(vec![[-1, -1], [-1, 0], [-1, 1], [0, 0]]);
        tile.positions_180 = Self::convert_positions(vec![[-1, 0], [0, 0], [1, 0], [0, 1]]);
        tile.positions_270 = Self::convert_positions(vec![[1, -1], [1, 0], [1, 1], [0, 0]]);

        tile
    }

    fn generate_s() -> Tile {
        let mut tile = Tile::empty_tile();

        tile.positions_0 = Self::convert_positions(vec![[-1, -1], [-1, 0], [0, 0], [0, 1]]);
        tile.positions_90 = Self::convert_positions(vec![[-1, 0], [0, 0], [0, -1], [1, -1]]);
        tile.positions_180 = tile.positions_0.clone();
        tile.positions_270 = Self::convert_positions(vec![[-2, 0], [-1, 0], [-1, -1], [0, -1]]);

        tile
    }

    fn generate_s_inverse() -> Tile {
        let mut tile = Tile::empty_tile();

        tile.positions_0 = Self::convert_positions(vec![[0, -1], [-1, 0], [0, 0], [-1, 1]]);
        tile.positions_90 = Self::convert_positions(vec![[-1, -1], [0, 0], [0, -1], [1, 0]]);
        tile.positions_180 = tile.positions_0.clone();
        tile.positions_270 = Self::convert_positions(vec![[-2, -1], [-1, 0], [-1, -1], [0, 0]]);

        tile
    }

    fn generate_l() -> Tile {
        let mut tile = Tile::empty_tile();

        tile.positions_0 = Self::convert_positions(vec![[0, -1], [0, 0], [0, 1], [-1, 1]]);
        tile.positions_90 = Self::convert_positions(vec![[-1, -1], [-1, 0], [0, 0], [1, 0]]);
        tile.positions_180 = Self::convert_positions(vec![[1, -1], [0, -1], [0, 0], [0, 1]]);
        tile.positions_270 = Self::convert_positions(vec![[-1, 0], [0, 0], [1, 0], [1, 1]]);

        tile
    }

    fn generate_l_inverse() -> Tile {
        let mut tile = Tile::empty_tile();

        tile.positions_0 = Self::convert_positions(vec![[0, -1], [0, 0], [0, 1], [1, 1]]);
        tile.positions_90 = Self::convert_positions(vec![[-1, 1], [-1, 0], [0, 0], [1, 0]]);
        tile.positions_180 = Self::convert_positions(vec![[-1, -1], [0, -1], [0, 0], [0, 1]]);
        tile.positions_270 = Self::convert_positions(vec![[-1, 0], [0, 0], [1, 0], [1, -1]]);

        tile
    }

    fn convert_positions(positions: Vec<[i16; 2]>) -> Vec<Vector2i> {
        positions.iter().map(|&position| position.into()).collect()
    }
}
