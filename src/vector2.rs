use std::fmt::{Display, Formatter, Result};
use std::ops::{Add, Sub};

pub type Vector2i = Vector2<i16>;
pub type Vector2f = Vector2<f64>;

#[derive(Copy, Clone, PartialEq)]
pub struct Vector2<T> {
    pub x: T,
    pub y: T,
}

impl<T> Display for Vector2<T> where T: Display {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

impl<T> Add for Vector2<T> where T: std::ops::Add<Output=T> {
    type Output = Vector2<T>;

    fn add(self, other: Self) -> Self::Output {
        Vector2 { x: self.x + other.x, y: self.y + other.y }
    }
}

impl<T> Sub for Vector2<T> where T: std::ops::Sub<Output=T> {
    type Output = Self;

    fn sub(self, other: Self) -> Self::Output {
        Vector2 { x: self.x - other.x, y: self.y - other.y }
    }
}

impl<T> From<[T; 2]> for Vector2<T> where T: Copy + Clone {
    fn from(array: [T; 2]) -> Self {
        Vector2 { x: array[0], y: array[1] }
    }
}
